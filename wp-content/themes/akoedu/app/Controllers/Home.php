<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Home extends Controller
{
	/**
   * @return string accessToken
   */
	public function accessToken()
	{
		return '12644706909.c0392c7.6befd2ddf1e84511baa08675f0f2f8c0';
	}

	/**
   * @return string Instagram user profile ID
   */
	public function igUserId()
	{
		return '12644706909';
	}

	/**
   * Add Instagram feed to the footer
   *
   * @return array $instagram_response
   */
	public function remoteWp()
	{
		$user_id 			= $this->igUserId();
		$access_token = $this->accessToken();
		if( !$user_id && !$access_token ) {
			return false;
		}
		$remote_wp = wp_remote_get( 'https://api.instagram.com/v1/users/' . $user_id . '/media/recent/?access_token=' . $access_token );
		return $remote_wp;
	}

	public function instagramResponse()
	{
		$remote_wp = $this->remoteWp();
		if( !$remote_wp ) {
			return false;
		}

		return $instagram_response = json_decode( $remote_wp['body'] );;
	}
}
