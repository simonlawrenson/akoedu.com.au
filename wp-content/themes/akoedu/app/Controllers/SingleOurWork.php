<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleOurWork extends Controller
{
	public function company()
	{
		return get_field('company');
	}
	public function goal()
	{
		return get_field('goal');
	}
	public function outcome()
	{
		return get_field('outcome');
	}
}
