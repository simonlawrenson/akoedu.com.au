<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
  /**
   * WordPress site name
   * @return string
   */
  public function siteName()
  {
    return get_bloginfo('name');
  }

  /**
   * Page title
   * @return string
   */
  public static function title()
  {
    if (is_home()) {
      if ($home = get_option('page_for_posts', true)) {
        return get_the_title($home);
      }
      return __('Latest Posts', 'akoedu');

    } elseif (is_tax() ){
      return single_term_title( '', false );

    } elseif (is_category() ){
      return single_cat_title( '', false );

    } elseif (is_archive()) {
      return post_type_archive_title( '', false );
    
    } elseif (is_search()) {
      return sprintf(__('Search Results for %s', 'akoedu'), get_search_query());
    } elseif (is_404()) {
      return __('Not Found', 'akoedu');
    }
    return get_the_title();
  }

  /**
   * Alter Pagination links
   * @return array
   */
  public function pagination()
  {
    if( is_home() || is_post_type_archive('our-work') ) {
      return $pagination = [
        'prev' => get_previous_posts_link('<i class="fas fa-angle-left"></i> Previous'),
        'next' => get_next_posts_link('Next <i class="fas fa-angle-right"></i>')
      ];
    } elseif( is_singular('post') ) {
      return get_option('page_for_posts');
    } elseif( is_singular('our-work') ) {
      return get_post_type_archive_link('our-work');
    }
    return;
  }

  /**
   * Get site logo or return site name
   * @return string
   */
  public function siteBranding() {
    $logo = get_field('site_logo', 'options');
    return ( $logo ? wp_get_attachment_image($logo, 'site-logo', false, ['class' => 'site-logo']) : get_bloginfo('name', 'display') );
  }

  /**
   * Primary Nav Menu arguments
   * @return array
   */
  public function primaryMenu() {
    $args = array(
      'theme_location' => 'primary_navigation',
      'container'       => 'div',
      'container_id'    => 'navbarNav',
      'container_class' => 'collapse navbar-collapse',
      'menu_class'      => 'nav navbar-nav ml-auto',
      'depth'           => 2,
      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
      'walker'            => new \App\wp_bootstrap4_navwalker(),
    );
    return $args;
  }

  /**
   * Prepare an array containing all ACF custom fields
   *
   * @return array $meta_data  Main social profiles for the website
   */
  public function customColumns()
  {
    $custom_columns = [];
    $custom_columns['padding'] = get_field('padding');
    $custom_columns['columns'] = get_field('columns');
    
    return $custom_columns;
  }

  /**
   * Prepare an array containing social links
   *
   * @return array $social  Main social profiles for the website
   */
  public function socialAccounts()
  {
    // $config = json_decode( file_get_contents( get_stylesheet_directory() . '/acf-json/' . $field_group_json ), true );
    // $meta_data = get_all_custom_field_meta( 'option', $config);
    
    $fb = get_field('social_fb', 'options');
    $tw = get_field('social_tw', 'options');
    $in = get_field('social_in', 'options');
    $yt = get_field('social_yt', 'options');
    $pi = get_field('social_pi', 'options');
    $li = get_field('social_li', 'options');
  
    // If there are any social profiles available create an array
    if( $fb || $tw || $in || $yt || $pi || $li ) {
      $social = [];
      ( $fb ? $social['Facebook']  = [ 'icon' => 'facebook-f', 'link' => 'https://facebook.com/'. $fb ] : false );
      ( $tw ? $social['Twitter']   = [ 'icon' => 'twitter',    'link' => 'https://twitter.com/'. $tw ] : false );
      ( $in ? $social['Instagram'] = [ 'icon' => 'instagram',  'link' => 'https://instagram.com/'. $in ] : false );
      ( $yt ? $social['Youtube']   = [ 'icon' => 'youtube',    'link' => 'https://youtube.com/user/'. $yt ] : false );
      ( $pi ? $social['Pinterest'] = [ 'icon' => 'pinterest-p','link' => 'https://www.pinterest.com.au/'. $pi ] : false );
      ( $pi ? $social['Linkedin']  = [ 'icon' => 'linkedin-in','link' => 'https://linkedin.com/'. $pi ] : false );
      return $social;
    }
  }

  /**
   * Prepare an array containing contact information
   *
   * @return array $contact  Main contact information for the website
   */
  public function contactInfo()
  {
    $address  = get_field('ct_address', 'options');
    $phone    = get_field('ct_tel', 'options');
    $email    = get_field('ct_email', 'options');

    // If there is any contact information available create an array
    if( $address || $phone || $email ) {
      $contact = [];
      ( $address ? $contact['address']  = $address : false );
      ( $phone ? $contact['phone']      = \App\clean_phone($phone) : false );
      ( $email ? $contact['email']      = $email : false );
      return $contact;
    }
  } 
}