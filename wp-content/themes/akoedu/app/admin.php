<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
  // Add postMessage support
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
  $wp_customize->selective_refresh->add_partial('blogname', [
    'selector' => '.brand',
    'render_callback' => function () {
        bloginfo('name');
    }
  ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
  wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});


/**
 * Disable Gutenberg Editor for post types
 */
add_filter('use_block_editor_for_post_type', function ( $use_block_editor, $post_type ) {
  if( $post_type === 'our-work' ) {
    return  false;
  } else {
    return $use_block_editor;
  }
}, 10, 2);

/*
 * Add ACF Options page
 */ 
add_action('acf/init', function() {
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
  }
});

/**
 * ACF save json folder
 */
add_filter('acf/settings/save_json', function( $path ) {
  // update path
  $path = get_stylesheet_directory() . '/acf-json';
  // return
  return $path;
});

/**
 * ACF load json folder
 */
add_filter('acf/settings/load_json', function($paths) {
    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $paths;
});