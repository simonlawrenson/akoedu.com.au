<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    // return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'akoedu') . '</a>';
    return ' &hellip;';
});
/*
 * Trim excerpt word count
 */
add_filter('excerpt_length',function () {
  return 30;
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/*
 * Add GTM code to head
 * @hook WordPress wp_head
 */
add_action('wp_head', function() {
  if( $_SERVER['HTTP_HOST'] === 'akoedu.com.au' || $_SERVER['HTTP_HOST'] === 'www.akoedu.com.au' ) :
    ob_start(); ?>
    <!-- Google Tag Manager -->
    <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PS2LH2B');
    </script>
    <!-- End Google Tag Manager -->
    <?php $output = ob_get_clean();
    echo $output;
  endif;
}, -10);


/*
 * Add GTM code to body
 * @hook Custom before_header
 */
add_action('before_header', function() {
  if( $_SERVER['HTTP_HOST'] === 'akoedu.com.au' || $_SERVER['HTTP_HOST'] === 'www.akoedu.com.au' ) :
    ob_start(); ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PS2LH2B"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php $output = ob_get_clean();
    echo $output;
  endif;
}, 0);

/**
 * Wrap core Gutenberg blocks in Bootstrap classes
 *
 * @param string $block_content HTML layout of the blocks
 * @param obj $block Object containing blocks in use on the page
 * @return string $block_content HTML layout of the blocks, wrapped in new divs
 */
add_filter('render_block', function( $block_content, $block ) {
  if( is_singular('post') ) {
    return $block_content;
  }

  // Array of blocks to wrap
  $block_in_use = [
    'core/quote',
    'core/button',
    'core/column',
    'core/heading',
    'core/latest-posts',
    'core/list',
    'core/image',
    'core/paragraph',
    'core/pullquote',
    'core/shortcode',
    'core/table',
    'core-embed/spotify',
  ];

  if( in_array($block['blockName'], $block_in_use) ) {
    // HTML opening wrap
    $block_wrap_open = '<div class="wp-block-container '. $block['blockName'] .'-container"><div class="container-fluid rc-container-fluid"><div class="row"><div class="col-12">';
    // HTML closing wrap
    $block_wrap_closing = '</div></div></div></div>';
    $block_content = $block_wrap_open . $block_content . $block_wrap_closing;

  } elseif( $block['blockName'] === 'core/media-text' ) {
    $block_wrap_open = '<div class="wp-block-container wp-media-text"><div class="container-fluid rc-container-fluid">';
    $block_wrap_closing = '</div></div>';
    $block_content = $block_wrap_open . $block_content . $block_wrap_closing;

  } elseif( $block['blockName'] === 'core/table' ) { 
    $block_wrap_open = '<div class="table-container block-container"><div class="container-fluid rc-container-fluid"><div class="row"><div class="col-12"><div class="table-responsive">';
    $block_wrap_closing = '</div></div></div></div></div>';
    $block_content = $block_wrap_open . $block_content . $block_wrap_closing;

  } elseif( $block['blockName'] === 'core-embed/youtube' ) { 
    $block_wrap_open = '<div class="video-container block-container"><div class="container-fluid rc-container-fluid"><div class="row"><div class="col-12 col-md-9 mr-md-auto ml-md-auto"><div class="table-responsive">';
    $block_wrap_closing = '</div></div></div></div>';
    $block_content = $block_wrap_open . $block_content . $block_wrap_closing;
    
  }
  return $block_content;
}, 10, 2 );