// import external dependencies
import 'jquery';
// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
// import the social icons
import { faFacebookF, faTwitter, faInstagram, faYoutube, faPinterestP, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
// add the imported icons to the library
library.add(faFacebookF, faTwitter, faInstagram, faYoutube, faPinterestP, faLinkedinIn);
// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import blog from './routes/blog';
import aboutUs from './routes/about';



/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Blog page
  blog,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
