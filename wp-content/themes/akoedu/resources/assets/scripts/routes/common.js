export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
		const banner = $('.banner');
		let bannerHeight = $(banner).outerHeight();
		let triggerPosition = bannerHeight + 48;
		
		// Add class to header when scrolling down
    $(window).scroll(function() {
			if( $(window).scrollTop() > triggerPosition + 61 && !banner.hasClass('scrolled') ) {
				banner.addClass('scrolled');
      } else if( $(window).scrollTop() < triggerPosition && banner.hasClass('scrolled') ) {
				banner.removeClass('scrolled');
      }
    }).scroll();
		
		function offsetAnchor() {
			if (location.hash.length !== 0) {
				let navHeight     = $('.banner').height();            
				window.scrollTo(window.scrollX, window.scrollY - navHeight, 'smooth');
			}
		}
		// JavaScript to be fired on all pages, after page specific JS is fired
		// Scroll window to position on load, minus navigation height
		$(document).on( 'click', 'a[href^="#"]', function(e) {
      if( $(this).parents('.columns-container').length > 0 ) {
				if ( navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/) ) {
					window.setTimeout(function() {
						offsetAnchor();
					}, 0);
				} else {
					e.preventDefault();
					let hash          = $(this).attr('href');
					let navHeight     = $('.banner').height();
					let target        = $(hash).offset().top;
					let scrollToArea  = target - navHeight;
					$('html, body').animate({
            scrollTop: scrollToArea - 48,
          }, 800 );
        }
      }
    });
    // Set the offset when entering page with hash present in the url
    window.setTimeout(offsetAnchor, 100);
  },
};
