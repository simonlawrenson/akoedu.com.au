import 'slick-carousel';
export default {
  init() {
    // JavaScript to be fired on the blog page
  },
  finalize() {
    // JavaScript to be fired on the blog page, after the init JS
    // Init Slick Carousel
    $('.instagram-feed-slider').slick({
			autoplay: true,
      autoplaySpeed: 2500,
      arrows: false,
      dots: false,
      infinite: true,
      variableWidth: true,
      slidesToShow: 4,
      speed: 1000,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
          },
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 331,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 231,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    });
  },
};
