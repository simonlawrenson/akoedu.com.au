@extends('layouts.app')

@section('content')

@include('partials.page-header')
<div class="our-work-container">
  <div class="container-fluid rc-container-fluid">
    @if(have_posts())
      @while(have_posts()) @php the_post() @endphp
        @include('partials.content-'. get_post_type() )
      @endwhile
      <div class="row nav-post-links">
        <div class="col-12 col-sm text-left">
          @php previous_posts_link('<i class="fas fa-angle-left"></i> Previous') @endphp
        </div>
        <div class="col-12 col-sm text-right">
          @php next_posts_link('Next <i class="fas fa-angle-right"></i>'); @endphp
        </div>
      </div>
    @else
      <div class="row">
        <div class="col-12">
          <div class="alert alert-warning">
            {{ __('Sorry, no results were found.', 'akoedu') }}
          </div> <!-- end .alert -->
        </div> <!-- end .col-12 -->
      </div> <!-- end .row -->
    @endif
  </div> <!-- end .container-fluid -->
</div> <!-- end .our-work-container -->
@endsection