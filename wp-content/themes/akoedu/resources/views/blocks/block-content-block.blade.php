{{--
  Title: Content Block
  Description: A full width content block.
  Category: layout
  Icon: text
  Keywords: content content block content area
  Mode: edit
  PostTypes: page post
--}}
<!-- group_5c1982fd936f0 -->

@if( get_field('c_title') || get_field('c_content') )
	<div class="block-container content-block-container padding-{{ get_field('padding') }}">
		<div class="container-fluid opt-container-fluid">
			<div class="row">
				<div class="col-12 col-md-9 mr-auto ml-auto content-container">
					@if( get_field('c_title') )
						<h3 class="content-title">{{ get_field('c_title') }}</h3>
					@endif
					@if( get_field('c_content') )
						<div class="content-content">{!! get_field('c_content') !!}</div>
					@endif
					@if( get_field('c_link') )
						<a href="{{ get_field('c_link') }}" title="Find out more" class="btn content-link">Find out more</a>
					@endif
				</div> <!-- /.col-12 -->
			</div> <!-- /.row -->
		</div> <!-- /.container-fluid -->
	</div> <!-- /.content-block-container -->
<?php endif; ?>