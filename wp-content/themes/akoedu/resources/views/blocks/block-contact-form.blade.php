{{--
  Title: Contact Form
  Description: A shortcode to insert a contact form
  Category: layout
  Icon: email-alt
  Keywords: contact form get in touch
  Mode: edit
  PostTypes: page post
--}}

				
@if( get_field('cf_shortcode') )
	<div class="block-container contact-form-container padding-{{ get_field('padding') }}">
		<div class="container-fluid rc-container-fluid">
			<div class="row">
				<div class="col-12 col-sm-10  col-md-7 mr-auto ml-auto">
					{!! get_field('cf_shortcode') !!}
				</div> <!-- /.col-12 -->
			</div> <!-- /.row -->
		</div> <!-- /.container-fluid -->
	</div> <!-- /.block-container -->
@endif