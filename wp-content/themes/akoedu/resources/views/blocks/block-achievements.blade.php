{{--
  Title: Multi Row Columns
  Description: Several rows of columns with supporting icons and content
  Category: layout
  Icon: slides
  Keywords: columns multi level achievements list icons
  Mode: edit
  PostTypes: page post
--}}

@if( get_field('row_title') || get_field('row_left_col') || get_field('row_right_col') || have_rows('row_cols') )
	<div class="block-container multi-row-columns-container padding-{{ get_field('padding') }}" id="{{$block['slug']}}">
		<div class="container-fluid rc-container-fluid">
			@if( get_field('block_title') )
				<div class="row">
					<div class="col-12">
						<h3 class="h2 block-title">{{ get_field('block_title') }}</h3>
					</div> <!-- /.col-12 -->
				</div> <!-- /.row -->
			@endif
			@if( get_field('row_title') || get_field('row_left_col') || get_field('row_right_col') )
				<div class="row multi-row-one">
					@if( get_field('row_title') || get_field('row_img') )
						<div class="col-12">
							{!! wp_get_attachment_image(get_field('row_img'), 'thumbnail', false, ['class' => 'col-img row-col-img']) !!}
							<h4 class="row-col-title">{{ get_field('row_title') }}</h4>
						</div> <!-- /.col-12 -->
					@endif

					@if( get_field('row_col') )
						<div class="col-12 col-sm-6 col-md-3 ml-auto mr-auto row-col">
							<div class="row-col-content">{!! get_field('row_col') !!}</div>
						</div> <!-- /.col-12 -->
					@endif

					@if( get_field('row_left_col') )
						<div class="col-12 col-sm-6 col-md-5 ml-auto row-col-left">
							<div class="row-col-content">{!! get_field('row_left_col') !!}</div>
						</div> <!-- /.col-12 -->
					@endif
					@if( get_field('row_right_col') )
						<div class="col-12 col-sm-6 col-md-5 mr-auto row-col-right">
							<div class="row-col-content">{!! get_field('row_right_col') !!}</div>
						</div> <!-- /.col-12 -->
					@endif
				</div> <!-- /.row -->
			@endif
				
			@if( have_rows('row_cols') )
				<div class="row multi-row-two">
					@while(have_rows('row_cols') ) @php the_row() @endphp
						<div class="col-12 col-sm-6 col-md-3 mr-auto ml-auto d-flex flex-column align-items-center col-container">
							{!! wp_get_attachment_image(get_sub_field('row_col_image'), 'thumbnail', false, ['class' => 'col-img row-col-img']) !!}
							@if( get_sub_field('row_col_title') )
								<h4 class="col-title">{{ get_sub_field('row_col_title') }}</h4>
							@endif
							@if( get_sub_field('row_col_content') )
								<div class="col-content">{!! get_sub_field('row_col_content') !!}</div>
							@endif
						</div> <!-- /.col-12 -->
					@endwhile
				</div> <!-- /.row -->
			@endif
		</div> <!-- /.container-fluid -->
	</div> <!-- /.block-container -->
@endif