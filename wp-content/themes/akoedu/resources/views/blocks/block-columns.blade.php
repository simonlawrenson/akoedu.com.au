{{--
  Title: Custom Columns
  Description: A varible number of columns that supports titles, images and content.
  Category: layout
  Icon: columns
  Keywords: columns content image title
  Mode: edit
  PostTypes: page post
--}}
<!-- group_5c19c781c83cc -->

				
@if( have_rows('columns') )
	<div class="block-container columns-container padding-{{ get_field('padding') }}">
		<div class="container-fluid rc-container-fluid">
			<div class="row">
				@while(have_rows('columns') ) @php the_row() @endphp
					<div class="col-12 col-sm-6 col-md-3 mr-auto ml-auto d-flex flex-column align-items-center col-container">
						{!! wp_get_attachment_image(get_sub_field('col_img'), 'thumbnail', false, ['class' => 'col-img']) !!}
						@if( get_sub_field('col_title') )
							<h4 class="col-title">{{ get_sub_field('col_title') }}</h4>
						@endif
						@if( get_sub_field('col_content') )
							<div class="col-content">{!! get_sub_field('col_content') !!}</div>
						@else
							<div class="col-no-content"></div>
						@endif
						@if( get_sub_field('col_link') )
							<a href="{!! get_sub_field('col_link') !!}" title="Find out more" class="btn col-link">Find out more</a>
						@endif
					</div> <!-- /.col-12 -->
				@endwhile
			</div> <!-- /.row -->
		</div> <!-- /.container-fluid -->
	</div> <!-- /.block-container -->
@endif