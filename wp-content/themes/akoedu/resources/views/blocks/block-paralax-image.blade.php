{{--
  Title: Paralax Image
  Description: A paralax image with overlayed text
  Category: layout
  Icon: slides
  Keywords: paralax image
  Mode: edit
  PostTypes: page post
--}}
<!-- group_5c19c781c83cc -->


@if( get_field('para_image') )
	<div class="paralax-container margin-{{ get_field('padding') }}" style="background-image: url({{get_field('para_image')}} );">
		@if( get_field('para_content') )
			<div class="paralax-content-container">
				<div class="container-fluid rc-container-fluid">
					<div class="row">
						<div class="col-12 paralax-content">
							<h4 class="h1">{{ get_field('para_content') }}</h4>
						</div> <!-- /.col-12 -->
					</div> <!-- /.row -->
				</div> <!-- /.container-fluid -->
			</div> <!-- /.paralax-content-container -->
		@endif
	</div> <!-- /.paralax-container -->
@endif