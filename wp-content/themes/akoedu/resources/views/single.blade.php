@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
		<div class="news-media-container news-media-single-container">
		  <div class="container-fluid rc-container-fluid">
    		@include('partials.content-single-'. get_post_type())

    		@if( $pagination )
	        <div class="row nav-post-links">
	          <div class="col-12 col-sm text-left">
	          	<a href="{!! get_permalink($pagination) !!}" title="Back to posts" class="btn">Back to posts</a>
	        	</div> <!-- end .col-12 -->
	        </div> <!-- end .row -->
	      @endif
		  </div> <!-- end .container-fluid -->
		</div> <!-- end .news-media-container -->
  @endwhile
@endsection