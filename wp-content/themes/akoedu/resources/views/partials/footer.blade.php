<footer class="content-info">
  <div class="container-fluid rc-container-fluid">
    <div class="row">
      @if( $social_accounts )
        <div class="col-12 social-container">
          @foreach($social_accounts as $platform => $account)
            <a href="{{ $account['link'] }}" title="{{ $platform }}" target="_blank" class="footer-social"><i class="fab fa-{{ $account['icon'] }}"></i></a>
          @endforeach
        </div> <!-- /.col-12 -->
      @endif
      <div class="col-12 contact-container">
        <p class="footer-contact"><a href="{{ home_url('/contact') }}" title="Contact us">Contact Us</a></p>
      </div> <!-- /.col-12 -->
    </div> <!-- /.row -->
  </div> <!-- /.container-fluid -->
</footer>
