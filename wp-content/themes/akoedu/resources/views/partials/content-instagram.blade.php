@if( $remote_wp['response']['code'] == 200 )
  <div class="instagram-feed-container">
    <div class="instagram-feed-slider">
      @foreach( $instagram_response->data as $m )
        <div class="slick-slide">
          <a href="{!! $m->link !!}" class="insta-link" target="_blank">
           <img src="{!! $m->images->standard_resolution->url !!}" title="{!! $m->caption->text !!}" class="slide-img" />
          </a>
          <i class="fab fa-instagram"></i>
        </div>
      @endforeach
    </div>
	</div> <!-- / .instagram-feed-container -->
@elseif( $remote_wp['response']['code'] == 400 )
   {!! '<b>' . $remote_wp['response']['message'] . ': </b>' . $instagram_response->meta->error_message; !!}
@endif