<article @php post_class() @endphp>
  <div class="row post-single-row">
    <div class="col-12">
      <header>
        <h1 class="entry-title">{!! get_the_title() !!}</h1>
        @include('partials/entry-meta')
      </header>
      <div class="entry-content">
        @php the_content() @endphp
      </div> <!-- end .entry-content -->
    </div> <!-- end .col-12 -->
  </div> <!-- end .row -->
</article>