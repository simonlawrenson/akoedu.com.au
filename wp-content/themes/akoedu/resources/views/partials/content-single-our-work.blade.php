<article @php post_class() @endphp>
	<div class="row post-row">
		<div class="col-12 col-md-6">
			@if( has_post_thumbnail() )
				<a href="{!! the_permalink() !!}" title="Read more">
					{!! the_post_thumbnail('blog-featured', ['class' => 'news-media-img']) !!}
				</a>
			@else
				<a href="{!! the_permalink() !!}" title="Read more">
					<img src="@asset('images/placeholder-alt.png')" alt="{{ $site_name }} Logo" class="placeholder news-media-img"/>
				</a>
			@endif
		</div> <!-- end .col -->

		<div class="col-12 col-md-6">
		  @if( $company ) <h5 class="h6 our-work-detail"><span class="label">Company:</span> <span class="h4">{!! $company !!}</span></h4> @endif
		  @if( $goal ) <h5 class="h6 our-work-detail"><span class="label">Goal:</span> <span class="h4">{!! $goal !!}</span></h4> @endif
		  @if( $outcome ) <h5 class="h6 our-work-detail"><span class="label">Outcome:</span> <span class="h4">{!! $outcome !!}</span></h4> @endif
		  <div class="entry-summary">
		    @php the_content() @endphp
		  </div>
		</div> <!-- end .col -->
	</div> <!-- end .row -->
</article>