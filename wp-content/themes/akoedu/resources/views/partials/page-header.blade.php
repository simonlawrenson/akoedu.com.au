<div class="page-header">
  <div class="container-fluid rc-container-fluid">
    <div class="row">
      <div class="col-12">
  			<h1>{!! App::title() !!}</h1>
      </div> <!-- end .col-12 -->
    </div> <!-- end .row -->
  </div> <!-- end .container -->
</div> <!-- end .page-header -->