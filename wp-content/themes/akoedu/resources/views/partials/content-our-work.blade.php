<article @php post_class() @endphp>
	<div class="row align-items-center post-row">
		<div class="col-12 col-md-6">
			@if( has_post_thumbnail() )
				<a href="{!! the_permalink() !!}" title="Read more">
					{!! the_post_thumbnail('blog-featured', ['class' => 'news-media-img']) !!}
				</a>
			@else
				<a href="{!! the_permalink() !!}" title="Read more">
					<img src="@asset('images/placeholder-alt.png')" alt="{{ $site_name }} Logo" class="placeholder news-media-img"/>
				</a>
			@endif
		</div> <!-- end .col -->

		<div class="col-12 col-md-6">
		  <header>
		    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
		  </header>
		  @if( $company ) <h5 class="h6 our-work-detail"><span class="label">Company:</span> <span class="h4">{!! $company !!}</span></h4> @endif
		  @if( $goal ) <h5 class="h6 our-work-detail"><span class="label">Goal:</span> <span class="h4">{!! $goal !!}</span></h4> @endif
		  @if( $outcome ) <h5 class="h6 our-work-detail"><span class="label">Outcome:</span> <span class="h4">{!! $outcome !!}</span></h4> @endif
		  <div class="entry-summary">
		    @php the_excerpt() @endphp
		    <a href="{!! the_permalink() !!}" title="Read more" class="btn btn-primary">Read more</a>
		  </div>
		</div> <!-- end .col -->
	</div> <!-- end .row -->
</article>