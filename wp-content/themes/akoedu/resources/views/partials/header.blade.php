<header class="banner sticky-top" role="banner">
  <div class="container-fluid">
    <nav class="nav-primary navbar navbar-expand-lg" role="navigation">
    	<a class="brand" href="{{ home_url('/') }}">{!! $site_branding !!}</a>
      
      @if (has_nav_menu('primary_navigation'))
	  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	      </button>
        {!! wp_nav_menu($primary_menu) !!}
      @endif
    </nav>
  </div>
</header>
