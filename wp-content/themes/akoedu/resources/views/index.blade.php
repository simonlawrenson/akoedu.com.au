@extends('layouts.app')

@section('content')

@include('partials.page-header')
<div class="news-media-container">
  <div class="container-fluid rc-container-fluid">
    <div class="row">
      <div class="col-12 col-md-9">
        @if(have_posts())
          @while(have_posts()) @php the_post() @endphp
            @include('partials.content-'. get_post_type() )
          @endwhile
          @if( $pagination )
            <div class="row nav-post-links">
              @if( $pagination['prev'] ) {!! '<div class="col-12 col-sm text-left">'. $pagination['prev'] .'</div>' !!} @endif
              @if( $pagination['next'] ) {!! '<div class="col-12 col-sm text-right">'. $pagination['next'] .'</div>' !!} @endif
            </div> <!-- end .row -->
          @endif
        @else
          <div class="alert alert-warning">
            {{ __('Sorry, no results were found.', 'akoedu') }}
          </div> <!-- end .alert -->
        @endif
      </div> <!-- end .col-12 -->
      <div class="col-12 col-md-3">
        <a href="https://twitter.com/intent/tweet?button_hashtag=edu_ako&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-lang="en" data-dnt="true" data-show-count="false">Tweet #edu_ako</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
      </div> <!-- end .col-12 -->
    </div> <!-- end .row -->
  </div> <!-- end .container-fluid -->
</div> <!-- end .news-media-container -->
@include('partials.content-instagram')
@endsection
