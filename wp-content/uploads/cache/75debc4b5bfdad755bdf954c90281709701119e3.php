
<!-- group_5c19c781c83cc -->


<?php if( get_field('para_image') ): ?>
	<div class="paralax-container margin-<?php echo e(get_field('padding')); ?>" style="background-image: url(<?php echo e(get_field('para_image')); ?> );">
		<?php if( get_field('para_content') ): ?>
			<div class="paralax-content-container">
				<div class="container-fluid rc-container-fluid">
					<div class="row">
						<div class="col-12 paralax-content">
							<h4 class="h1"><?php echo e(get_field('para_content')); ?></h4>
						</div> <!-- /.col-12 -->
					</div> <!-- /.row -->
				</div> <!-- /.container-fluid -->
			</div> <!-- /.paralax-content-container -->
		<?php endif; ?>
	</div> <!-- /.paralax-container -->
<?php endif; ?>