# [AKO](https://www.akoedu.com.au) by [lawrenson.dev](https://lawrenson.dev)

This project is a custom WordPress theme built by Simon Lawrenson. This theme uses [Sage 9.0.9](https://github.com/roots/sage) as a starter theme.

## Features

* Sass for stylesheets
* Modern JavaScript
* [Theme wrapper](https://roots.io/sage/docs/theme-wrapper/)
* [Webpack](https://webpack.github.io/) for compiling assets, optimizing images, and concatenating and minifying files
* [Browsersync](http://www.browsersync.io/) for synchronized browser testing
* [Blade](https://laravel.com/docs/5.6/blade) as a templating engine
* [Controller](https://github.com/soberwp/controller) for passing data to Blade templates
* CSS framework (optional): [Bootstrap 4](https://getbootstrap.com/), [Bulma](https://bulma.io/), [Foundation](https://foundation.zurb.com/), [Tachyons](http://tachyons.io/), [Tailwind](https://tailwindcss.com/)


## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| WordPress >= 4.7| `N/A`        | [wordpress.org](https://en-au.wordpress.org/download/) |
| PHP >= 7.1.3    | `php -v`     | [php.net](https://php.net/manual/en/install.php) |
| Composer        | `composer -v`| [getcomposer.org](https://getcomposer.org/download/) |
| Node.js >= 8.0.0| `node -v`    | [nodejs.org](https://nodejs.org/) |
| Yarn            | `yarn -v`    | [yarnpkg.com](https://classic.yarnpkg.com/en/docs/install#mac-stable) |

# Setup you local development environment
## Valet
```shell
$ Enter Valet instructions here
$ Enter Valet instructions here
$ Enter Valet instructions here
```

## Setup local development project

```shell
$ 'mkdir /Websites/files/directory' - Create your new local development environment directory
$ 'cd /Website/files/directory' - Navigation to your local development directory
$ git init - initialize a new empty repo
```

## Cloning files from Bitbucket [Recommended]

Add your SSH key to your Bitbucket account. For help setting this up check out the Bitbucket article [article](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html#SetupanSSHkey-ssh2SetupSSHonmacOS/Linux).

1. From Bitbucket, choose Bitbucket settings from your avatar in the lower left. The Account settings page opens.
2. Click SSH keys. If you've already added keys, you'll see them on this page.
3. In your terminal window, copy the contents of your public key file. If you renamed the key, replace id_rsa.pub with the public key file name.
On macOS, the following command copies the output to the clipboard:
```shell
$  pbcopy < ~/.ssh/id_rsa.pub
```
4. Select and copy the key output in the clipboard. If you have problems with copy and paste, you can open the file directly with Notepad. Select the contents of the file (just avoid selecting the end-of-file characters).
5. From Bitbucket, click Add key.
6. Enter a Label for your new key, for example, Default public key.
7. Paste the copied public key into the SSH Key field. You may see an email address on the last line when you paste. It doesn't matter whether or not you include the email address in the Key.
8. Bitbucket sends you an email to confirm the addition of the key.

Then from your Terminal window, git clone the repo you want to edit into your local dev directory.

```shell
$ 'git clone git@bitbucket.org:simonlawrenson/my-repo-name.git .' - Clone files into this direcrory
```

## Import Database
```shell
$ Enter DB instructions here
$ Enter DB instructions here
$ Enter DB instructions here
```

# Theme installation
## Creating a new project

```shell
$ 'cd Websites/my-local-project.com.au/wp-content/themes' - Navigate to the themes directory
$ 'composer create-project roots/sage my-new-theme-name' - Create and download a new Sage 9.0.9 project
$ cd /my-new-theme-name - Navigate to the theme directory
$ 'yarn' - Install dependancies
$ 'yarn start' - Compile assets when file changes are made, start Browsersync session
```

## Installing an existing project

```shell
$ 'cd Websites/my-local-project.com.au/wp-content/theme/my-existing-theme-name' - Navigate to the theme directory
$ 'yarn' - Install dependancies
$ 'yarn start' - Compile assets when file changes are made, start Browsersync session
```

### Available yarn commands

* 'yarn' - Install dependancies
* 'yarn start' - Compile assets when file changes are made, start Browsersync session
* 'yarn build' - Compile and optimize the files in your assets directory
* 'yarn build:production' - Compile assets for production

### Using BrowserSync

To use BrowserSync during `yarn start` you need to update `devUrl` at the bottom of `assets/manifest.json` to reflect your local development hostname.

For example, if your local development URL is `http://project-name.localhost` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://project-name.localhost"
  }
...
```
If your local development URL looks like `http://localhost:3000/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:3000/project-name/"
  }
...
```
# Deployment
```
$ # Navigation to the root directory
$ 'git add ...' - Stage the files for deployment 
$ 'git commit -m "Deployment message here"' - Commit the changes
$ 'git remote -v' - Check the remotes available
$ 'git push remote-name master' - Push changes back to the remote
```

```shell
$ Enter Deploybot instructions here
$ Enter Deploybot instructions here
$ Enter Deploybot instructions here
```